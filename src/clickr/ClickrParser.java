package clickr;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.regex.*;

public class ClickrParser {

    public ClickrParser() {
        try {
            _robot = new Robot();
        }
        catch (AWTException e) {

        }
    }

    public Boolean parseLine(String line) {
      Matcher commandMatcher = (Pattern.compile(_commandRegExp)).matcher(line);

      if (commandMatcher.matches()) {
          if (commandMatcher.group(1).equals("click") || commandMatcher.group(1).equals("klik")) {
              _robot.mouseMove(Integer.parseInt(commandMatcher.group(2)), Integer.parseInt(commandMatcher.group(3)));
              _robot.mousePress(InputEvent.BUTTON1_MASK);
              pause(10);
              _robot.mouseRelease(InputEvent.BUTTON1_MASK);
          }
          else if (commandMatcher.group(1).equals("pause") || commandMatcher.group(1).equals("pauza")) {
              pause(Integer.parseInt(commandMatcher.group(2)));
          }
          return true;
      }
      return false;
    }

    private void pause(int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        }
            catch (Exception e) {
        }
    }

    private Robot _robot;
    private String _commandRegExp = "(\\w+)\\s+(\\d+)\\s*,*\\s*(\\d*)";
}
